$(document).ready(function () {
    //Definindo máscara para telefones
    $('.tel_ddd').mask('(00) 00000-0000');

});

let deletarContato = function (id) {

    swal("Deseja realmente deletar este contato?", {
        buttons: {
            cancel: "Não!",
            confirm: {
                text: "Sim!",
                value: 1,
            },
        },
    }).then((value) => {
        if (value == 1) {
            $.ajax({
                type: 'GET',
                dataType: 'JSON',
                url: '/contact/delete/' + id,
                data: {id: id},
                success: function (txt) {
                    if (txt == 0) {
                        $('#tr-' + id).fadeOut('slow', function () {
                            $(this).remove();
                            swal("Contato deletado com sucesso!");
                        });
                    } else {
                        swal('Falha ao tentar deletar contato, Por favor tente novamento!');
                    }
                }
            })

        }
    });

};