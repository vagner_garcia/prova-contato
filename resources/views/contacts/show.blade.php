@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Contato: {{ $contact->name }}</h2>
        <p>E-mail: {{ $contact->email }}</p>
        <p>Telefone: {{ $contact->phone }}</p>
        <p>Criado Em: {{ date_format($contact->created_at, 'd/m/Y H:i:s') }}</p>
        <p>Alterado Em: {{ date_format($contact->updated_at, 'd/m/Y H:i:s') }}</p>
        <a href='{!! url('/contacts'); !!}'>
            <button class="btn btn-primary"><i class="fa fa-arrow-left"></i> Voltar</button>
        </a>
    </div>

@endsection