@extends('layouts.app')

@section('content')
    <div class="container">

        {!! Form::open(array('method' => 'get', 'action' => 'ContactsController@index')) !!}

        <div class="row">
            <div class="form-group col-md-3">
                {!! Form::text('search', null,
                        array('placeholder'=>'Buscar por Nome', 'class' => 'form-control')) !!}
            </div>

            <div class="field">
                {!! Form::submit('Buscar',
                  array('class' => 'btn btn-success')) !!}
            </div>
        </div>
        {!! Form::close() !!}

        <div class="clearfix"></div>
        <div>
            <a href='{!! url('/contact'); !!}'>
                <button class="btn btn-primary"><span class="fa fa-plus"></span> Cadastrar Contato</button>
            </a>
        </div>

        @if(sizeof($contacts) == 0)
            <div class="alert alert-warning">
                Nenhum fomento encontrado.
            </div>
        @endif

        <table class="table">
            <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Telefone</th>
                <th scope="col">E-mail</th>
                <th scope="col">Opções</th>
            </tr>
            </thead>
            <tbody>
            @foreach($contacts as $contact)
                <tr id="tr-{{$contact->id}}">
                    <td>{{$contact->name}}</td>
                    <td>{{$contact->phone}}</td>
                    <td>{{$contact->email}}</td>
                    <td>
                        <a href='{!! url('/contact/edit/'.$contact->id); !!}'>
                            <button class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></button>
                        </a>
                        <a href='{!! url('/contact/'.$contact->id); !!}'>
                            <button class="btn btn-warning btn-sm"><i class="fa fa-list"></i></button>
                        </a>
                        <button onclick="deletarContato({{$contact->id}})" class="btn btn-danger btn-sm"><i
                                    class="fa fa-trash"></i></button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
