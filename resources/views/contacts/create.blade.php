@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Cadastrar Contato</h2>
        @if ($errors->any())
            <div class="alert alert-danger">
                Falha ao salvar o contato.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <a href='{!! url('/contacts'); !!}'>
            <button class="btn btn-primary"><i class="fa fa-arrow-left"></i> Voltar</button>
        </a>
        <div class="spaceBottom">
            <div class="row">
                {!! Form::open(array('action' => 'ContactsController@store', 'method' => 'put')) !!}
                <div class="form-group col-md-3">
                    {!! Form::label('name', 'Nome'); !!}
                    {!! Form::text('name', '', array('class' => 'form-control', 'placeholder' => 'Ex.: João')); !!}
                </div>

                <div class="form-group col-md-3">
                    {!! Form::label('email', 'E-mail'); !!}
                    {!! Form::text('email', '', array('class' => 'form-control', 'placeholder' => 'Ex.: joao@gmail.com')); !!}
                </div>

                <div class="form-group col-md-3">
                    {!! Form::label('phone', 'Telefone'); !!}
                    {!! Form::text('phone', '', array('class' => 'form-control tel_ddd' , 'placeholder' => 'Ex.: (67)99999-9999')); !!}
                </div>

                <div class="form-group col-md-12">

                    {!! Form::submit('Cadastrar', array('class' => 'btn btn-success')); !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection