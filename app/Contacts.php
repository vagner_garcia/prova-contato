<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'contacts';
    protected $fillable = array('name', 'phone', 'email', 'user_id');
    public $timestamps = true;
}
