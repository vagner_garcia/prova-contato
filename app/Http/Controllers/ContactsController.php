<?php

namespace App\Http\Controllers;

use App\Contacts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request = null)
    {


        if (!empty($request['search'])) {
            $name = $request['search'];
            $contacts = Contacts::where('name', 'like', "%$name%")
                ->where('user_id', '=', Auth::id())
                ->get();
        } else {
            $contacts = Contacts::all()
                ->where('user_id', '=', Auth::id());
        }

        return View::make('contacts.index')
            ->with('contacts', $contacts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'phone' => 'required:max:50'
        ]);

        Contacts::create([
            'name' => $request['name'],
            'phone' => $request['phone'],
            'email' => $request['email'],
            'user_id' => Auth::id()
        ]);

        return redirect()
            ->action('ContactsController@index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = Contacts::all()
            ->find($id);

        return View::make('contacts.show')
            ->with('contact', $contact);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = Contacts::all()
            ->find($id);

        return View::make('contacts.edit')
            ->with('contact', $contact);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'phone' => 'required:max:50'
        ]);

        $contact = Contacts::all()
            ->find($id);
        $contact->name = $request['name'];
        $contact->phone = $request['phone'];
        $contact->email = $request['email'];
        $contact->user_id = Auth::id();
        $contact->save();

        return redirect()
            ->action('ContactsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Contacts::destroy($id)) {
            $res = 0;
        } else {
            $res = 1;
        }

        return json_encode($res);
    }
}
